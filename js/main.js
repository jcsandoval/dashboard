$(document).ready(function () {
    $('.menu ul li:has(ul)').click(function (e) {
        e.preventDefault;

        if ($(this).hasClass('activado')) {
            $('.menu ul li ul li').css("margin-top", "0px");
            $(this).removeClass('activado');
            $(this).children('ul').slideUp();

        } else {
            $('.menu ul li ul li:nth-child(1)').css("margin-top", "3px");
            $('.menu ul li ul').slideUp();
            $('.menu ul li').removeClass('activado');
            $(this).addClass('activado');
            $(this).children('ul').slideDown();
        }
    });

    $(".grupo-usuario").click(function () {
        $(".menu-usuario").fadeToggle();
    });

    $("#hideMenu").click(function (e) {
        e.preventDefault();

        if ($("#hideMenu i").hasClass("oculto")) {
            $(".menu").css("display", "block");
            $(".panel").css("grid-column", "panel");
            $("#hideMenu i").removeClass("oculto");
        } else {
            $(".menu").css("display", "none");
            $(".panel").css("grid-column", "span 2");
            $("#hideMenu i").addClass("oculto");
        }
    });

    $(".bloque .bloque-head .bloque-iconos i:nth-child(2)").click(function (e) {
        e.preventDefault();

        var bloque = $(this).parent().parent().parent();

        if ($(this).hasClass("fas fa-angle-down")) {
            bloque.animate({
                height: 60
            }, 300);
            $(this).removeClass("fas fa-angle-down").addClass("fas fa-angle-up");
        } else {
            bloque.css("height", "auto");
            $(this).removeClass("fas fa-angle-up").addClass("fas fa-angle-down");
        }
    });
});